export const ConnectionEnum = Object.freeze({
    NEW: 'new',
    CONNECTED: 'connected',
    DISCONNECTED: 'disconnected'
});

export const StateEnum = Object.freeze({
	WAITING: 'waiting',
	QUESTIONS: 'questions',
	SCORES: 'scores'
});