<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Quiz</title>

        <link href="{{ mix('/css/app.css') }}" rel="stylesheet">
    </head>
    <body class="text-center">
        <template id="app">test</template>

        <script>
            window.config = {
                socket: '{{ config('socket.host') }}'
            };
        </script>
        <script src="{{ mix('/js/app.js') }}"></script>
    </body>
</html>
