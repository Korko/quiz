<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Quiz Admin</title>

        <link href="{{ mix('/css/app.css') }}" rel="stylesheet">
    </head>
    <body class="text-center">
        <div id="app"></div>

        <script>
            window.config = {
                socket: '{{ config('socket.host') }}',
                admin: '{{ config('socket.admin') }}'
            };
        </script>
        <script src="{{ mix('/js/admin.js') }}"></script>
    </body>
</html>
