<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Bind server host
    |--------------------------------------------------------------------------
    |
    | This value usually is the external IP address of your server,
    | followed by the port number it's listening to. In some cases,
    | you may want to have a front end proxy (e.g. Nginx) to handle
    | the connection between client and server. In these cases, you
    | may want to bind the PHP server to localhost IP and a special
    | port. For example having Nginx handle the TLS/SSL on port 443
    | and the server on locl port only 8889.
    |
    */

    'server' => env('SOCKET_SERVER', 'ws://127.0.0.1:8889'),

    /*
    |--------------------------------------------------------------------------
    | External server host
    |--------------------------------------------------------------------------
    |
    | This value is the URI to connect to the server with a client.
    | Usually, it is the same value as the bind server host, but in
    | special cases like for the previous configuration, this value
    | can be different. If your clients are not in the same network
    | as the server is, this value can be either: one of the server
    | external IP address or one of the server host name.
    |
    */

    'host' => env('SOCKET_HOST', 'ws://127.0.0.1:8889'),

    /*
    |--------------------------------------------------------------------------
    | Admin's secret pass
    |--------------------------------------------------------------------------
    |
    | In order to identify the admin from any other users, we use a
    | shared secret value the admin client send.
    |
    */

    'admin' => env('SOCKET_ADMIN'),

];
