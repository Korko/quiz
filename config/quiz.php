<?php

return [
    /**
     * wording: Required string, Text of the question
     * answers: Required array, List of answers
     * solution: Required array|int, Index of List of indexes of correct answers
     * resolution: Optional string, Default 'all', Resolution solution (of one "all/one")
     * details: Optional string, Default empty string, Text to display as a proof of the solution
     * points: Optional integer, Default 1, Amount of points to give for that correct answer
     */
    'questions' => [
        [
            'wording' => 'Quelle est la couleur du cheval blanc d\'Henry IV ?',
            'answers' => ['Vert', 'Rouge', 'Blanc', 'La réponse D'],
            'solution' => [2, 3],
            'details' => <<<EOT
<img src="https://www.humour.com/medias/humoriste/2014/20140105155041.png" />
EOT
,// End of HEREDOC is in a line AFTER
            'points' => 2,
        ],
        [
            'wording' => 'Sous quel autre nom connait-on la mer des caraïbes ?',
            'answers' => ['La mer noire', 'La mer noire', 'La mer noire'],
            'solution' => [0, 1, 2],
            'resolution' => 'one',
            'details' => <<<EOT
<iframe width="560" height="315" src="https://www.youtube.com/embed/0SdcfsD_WVE" framboiser="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
EOT
,
        ],
    ],
];
