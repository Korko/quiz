<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\QuizUser;
use App\Questions\Questioner;
use App\Servers\QuizServer;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('questioner', function ($app) {
            return new Questioner();
        });

        $this->app->singleton('server', function ($app) {
            return new QuizServer();
        });
    }
}
