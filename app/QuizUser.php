<?php

namespace App;

use App\Questions\Question;
use App\Questions\QuestionsIterator;
use Questioner;
use Server;

class QuizUser extends User
{
    protected $isAdmin = false;
    protected $answers = [];

    public function isAdmin(bool $isAdmin = null): bool
    {
        if (isset($isAdmin)) {
            $this->isAdmin = $isAdmin;
        }
        return $this->isAdmin;
    }

    public function addAnswer(Question $question, array $answers): void
    {
        $this->answers[$question->id] = $answers;
    }

    public function hasAnswered(Question $question): bool
    {
        return !empty($this->answers[$question->id]) &&
            count($this->answers[$question->id]) === $question->requiredAnswers();
    }

    public function getAnswers(): array
    {
        $questions = new QuestionsIterator();
        $answers = $this->answers;
        foreach ($questions as $questionId => $question) {
            $answers[$questionId] = array_get($answers, $questionId, []);
        }

        return $answers;
    }

    public function isPending(): bool
    {
        return !$this->isConnected() && ($this->getDisconnectDelay() < 5 * 60);
    }

    public function getScore(): int
    {
        $score = 0;

        $questions = new QuestionsIterator();
        $answers = $this->getAnswers();
        foreach ($questions as $questionId => $question) {
            if ($answers[$questionId] !== []) {
                $isCorrectAnswer = $question->isCorrectAnswer($answers[$questionId]);
                if ($isCorrectAnswer) {
                    $score += $question->points;
                }
            }
        }

        return $score;
    }

    public function copy(User $user): void
    {
        parent::copy($user);

        // Copy admin status
        $this->isAdmin($user->isAdmin());

        // Copy answers
        foreach ($user->getAnswers() as $questionId => $answers) {
            $this->addAnswer(Questioner::getQuestion($questionId), $answers);
        }
    }
}
