<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class Questioner extends Facade
{
    protected static function getFacadeAccessor() { return 'questioner'; }
}