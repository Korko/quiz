<?php

namespace App;

use App\Interfaces\Connectable as IConnectable;
use App\Traits\Connectable;
use App\Traits\Observable;
use Hoa\Websocket\Node;
use Server;

class User implements IConnectable
{
    use Observable, Connectable {
        Connectable::connect as parentConnect;
        Connectable::disconnect as parentDisconnect;
    }

    private $_id = null;
    protected $_name = null;
    protected $_node = null;

    protected $discoTimer = null;

    public function __construct(string $id)
    {
        $this->_id = $id;
    }

    public function connect(Node $node): void
    {
        $this->discoTimer = null;
        $this->_node = $node;
        $this->parentConnect();
    }

    public function disconnect(): void
    {
        $this->discoTimer = time();
        $this->_node = null;
        $this->parentDisconnect();
    }

    public function getId(): ?string
    {
        return $this->_id;
    }

    public function setName(?string $newName): void
    {
        $oldName = $this->_name;
        $this->_name = $newName;

        $this->trigger('name', $oldName, $newName);
    }

    public function getName(): ?string
    {
        return $this->_name;
    }

    public function getNode(): ?Node
    {
        return $this->_node;
    }

    public function getDisconnectDelay(): int
    {
        return $this->discoTimer === null ? 0 : time() - $this->discoTimer;
    }

    public function copy(User $user): void
    {
        // Copy admin status
        $this->setName($user->getName());
    }
}