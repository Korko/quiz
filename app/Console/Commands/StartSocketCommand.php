<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Server;

class StartSocketCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'socket:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Start the socket used to synchronize clients';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Server::start(config('socket.server'));
    }
}
