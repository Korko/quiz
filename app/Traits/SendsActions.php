<?php

namespace App\Traits;

use App\Exceptions\ActionNotFoundException;

trait SendsActions
{
    public function __call(string $name, array $arguments): void
    {
        $className = 'App\\Servers\\Actions\\'.ucfirst($name);
        if (class_exists($className)) {
            (new $className(array_shift($arguments)))->exec(...$arguments);
        } else {
            throw new ActionNotFoundException($name);
        }
    }
}