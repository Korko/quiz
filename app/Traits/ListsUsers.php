<?php

namespace App\Traits;

use App;
use Hoa\Event\Bucket;
use User;

trait ListsUsers
{
    /**
     * List of all currently connected users
     *
     */
    protected $users = [];

    /**
     *
     *
     * @param  string $userId
     * @return App\Interfaces\User|null
     */
    public function addUser(string $userId): ?User
    {
        if ($this->hasUser($userId)) {
            return null;
        }

        $user = new User($userId);
        $this->setUser($userId, $user);

        return $user;
    }

    /**
     *
     *
     * @param  string   $userId
     * @param  App\Interfaces\User $user
     * @return void
     */
    protected function setUser(string $userId, User $user): void
    {
        $this->users[$userId] = $user;
    }

    /**
     *
     *
     * @param  string $userId
     * @return void
     */
    public function removeUser(string $userId): void
    {
        unset($this->users[$userId]);
    }

    /**
     *
     *
     * @param  string $userId
     * @return bool
     */
    public function hasUser(string $userId): bool
    {
        return isset($this->users[$userId]);
    }

    /**
     *
     *
     * @param  string $userId
     * @return App\Interfaces\User|null
     */
    public function getUser(string $userId): ?User
    {
        return $this->users[$userId] ?? null;
    }

    /**
     *
     *
     * @return array
     */
    public function getAllUsers(): array
    {
        return $this->users;
    }
}
