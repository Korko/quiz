<?php

namespace App\Traits;

use Closure;

trait Observable
{
	protected $binds = [];

	public function bind(string $property, Closure $callback): string
	{
		$bindId = uniqid();
		$this->binds[$property][$bindId] = $callback;
		return $bindId;
	}

	public function unbind(string $property, string $bindId): bool
	{
		if (isset($this->binds[$bindId])) {
			unset($this->binds[$bindId]);
			return true;
		}
		return false;
	}

	public function trigger(string $property, $oldValue, $newValue)
	{
		foreach (array_get($this->binds, $property, []) as $callback) {
			$callback($this, $oldValue, $newValue);
		}
	}
}