<?php

namespace App\Traits;

use App\Interfaces\Connection;

trait Connectable
{
    private $_state = Connection::DISCONNECTED;

    public function connect()
    {
        $this->_state = Connection::CONNECTED;
    }

    public function isConnected()
    {
        return $this->_state === Connection::CONNECTED;
    }

    public function disconnect()
    {
        $this->_state = Connection::DISCONNECTED;
    }

    public function isDisconnected()
    {
        return $this->_state === Connection::DISCONNECTED;
    }
}