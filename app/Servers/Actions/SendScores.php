<?php

namespace App\Servers\Actions;

use App\Questions\QuestionsIterator;
use Server;
use User;

class SendScores extends GenericAction
{
    public function exec(): void
    {
        // Compute score for everyone and sort users
        $scores = collect(Server::getPlayingUsers())->map(function (User $user) {
            $answers = $user->getAnswers();
            $score = $user->getScore();

            return array(
                'score' => $score,
                'answers' => $answers
            );
        })->sortByDesc('score');

        // Compute position (if ex-aequo, same position)
        $scores = $scores->map(function ($score) use ($scores) {
            return $score + array(
                'position' => $scores->filter(function ($score2) use ($score) {
                    return $score2 > $score;
                })->count() + 1
            );
        });

        $this->trigger->send('scores', [
            'scores' => $scores,
            'questions' => QuestionsIterator::getAllQuestions(),
        ]);
    }
}
