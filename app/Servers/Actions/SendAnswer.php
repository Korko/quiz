<?php

namespace App\Servers\Actions;

use Questioner;
use Server;
use User;

/**
 * Send to a user the current state of the quiz.
 * Including all the logic like current question or score.
 * Usually after a login to sync client to server.
 */
class SendAnswer extends GenericAction
{
    public function exec(): void
    {
        $question = Questioner::getCurrent();

        $this->trigger
            ->send('solution', [
                'question' => $question->id,
                'solution' => $question->solution,
                'details' => $question->details
            ]);
    }
}
