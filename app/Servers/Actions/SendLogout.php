<?php

namespace App\Servers\Actions;

use Server;
use User;

/**
 * Send the information to the admin saying
 * a new player just logged in
 */
class SendLogout extends GenericAction
{
	public function exec(User $user): void
	{
        $this->trigger->send('playerDisconnection', [
            'user' => [
                'id' => $user->getId(),
                'name' => $user->getName()
            ]
        ]);
	}
}