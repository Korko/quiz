<?php

namespace App\Servers\Actions;

use App\Servers\ActionTrigger;
use User;

abstract class GenericAction
{
	protected $trigger;

        /**
         * @param ?User[] $users
         */
	public function __construct(?array $users = null)
	{
		$this->trigger = new ActionTrigger($users);
	}
}
