<?php

namespace App\Servers\Actions;

use App\Questions\Question;
use Questioner;
use Server;
use User;

/**
 * Send the current question details
 * to a participant.
 */
class SendQuestion extends GenericAction
{
    public function exec(Question $question): void
    {
        Server::say('question '.$question->id);

        $this->trigger->send('question', function (?User $user) use ($question) {
            $answer = [];
            if($user) {
                $answer = array_get($user->getAnswers(), $question->id, []);
            }

            return [
                'question' => [
                    'id'              => $question->id,
                    'category'        => $question->category,
                    'wording'         => $question->wording,
                    'answers'         => $question->answers,
                    'answer'          => $answer,
                    'requiredAnswers' => $question->requiredAnswers(),
                    'hasNext'         => Questioner::hasMore()
                ]
            ];
        });
    }
}
