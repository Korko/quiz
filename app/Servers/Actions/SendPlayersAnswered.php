<?php

namespace App\Servers\Actions;

use App\Questions\Question;
use Server;
use User;

class SendPlayersAnswered extends GenericAction
{
    public function exec(Question $question): void
    {
        $users = collect(Server::getPlayingUsers());
        $usersAnswered = $users->filter(function (User $user) use ($question) {
            return $user->hasAnswered($question);
        });

        $this->trigger->send('playersAnswered', [
            'question'      => $question->id,
            'totalAnswered' => count($usersAnswered)
        ]);
    }
}
