<?php

namespace App\Servers\Actions;

use Questioner;
use Server;
use User;

/**
 * Send the current question details
 * to a participant.
 */
class SendCurrentQuestion extends GenericAction
{
    public function exec(): void
    {
        if (Server::isQuizStarted()) {
            $this->trigger->sendQuestion(Questioner::getCurrent());
        }
    }
}