<?php

namespace App\Servers\Actions;

use Server;
use User;

/**
 * Send to a user the current state of the quiz.
 * Including all the logic like current question or score.
 * Usually after a login to sync client to server.
 */
class SendCurrentState extends GenericAction
{
    protected $actionName = 'state';

    public function exec(): void
    {
    	// Depending on the current state of the quiz,
    	// Send the current question or the score
        if (Server::isQuizStarted()) {
            $this->trigger->sendCurrentQuestion();
        } elseif (Server::isQuizFinished()) {
            $this->trigger->sendScores();
        }

        // Always send the state value after the according data!
        $this->trigger->send('state', [
            'state' => Server::getCurrentState()
        ]);
    }
}
