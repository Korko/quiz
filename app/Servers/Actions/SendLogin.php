<?php

namespace App\Servers\Actions;

use Server;
use User;

/**
 * Send the information to the admin saying
 * a new player just logged in
 */
class SendLogin extends GenericAction
{
    public function exec(User $user): void
    {
        $this->trigger->send('playerConnection', [
            'user' => [
                'id' => $user->getId(),
                'name' => $user->getName()
            ]
        ]);
    }
}
