<?php

namespace App\Servers\Actions;

use Questioner;
use Server;

/**
 * Send to all participant
 * the next question if there's any
 */
class SendNextQuestion extends GenericAction
{
    public function exec(): void
    {
        if (Questioner::hasMore()) {
            Server::say('next question');
            $this->trigger->sendQuestion(Questioner::getNext());
        } else {
        	Server::endQuiz();
        }
    }
}