<?php

namespace App\Servers;

use App\Exceptions\EventNotFoundException;
use App\Interfaces\Connectable;
use App\Traits\ListsUsers;
use Hoa\Websocket\Node;
use Hoa\Websocket\Server as WebServer;
use User;

/**
 * Websocket server
 * with listing users
 */
class ExchangeServer extends WebsocketServer
{
    use ListsUsers;

    /**
     * Get the WebServer instance with the correct settings
     *
     * @param  string    $host URL of the server and port to listen
     * @return Hoa\Websocket\Server
     */
    protected function getServerInstance(string $host): WebServer
    {
        $websocket = parent::getServerInstance($host);
        $websocket->getConnection()
            ->setNodeName(SocketNode::class);

        return $websocket;
    }

    /**
     * Get the Event class name
     *
     * @param  string $eventName
     * @return string
     * @throw EventNotFoundException
     */
    protected function getEventClassName($eventName): string
    {
        try {
            return parent::getEventClassName($eventName);
        } catch(EventNotFoundException $e) {
            $user = $this->getCurrentUser();

            if ($user->isAdmin()) {
                return parent::getEventClassName('Admin\\'.ucfirst($eventName));
            } else {
                throw $e;
            }
        }
    }

    /**
     * Get current connected usser
     *
     * @return App\Interfaces\User
     */
    public function getCurrentUser(): User
    {
        $node = $this->getCurrentNode();
        return $this->getUser($node->getId());
    }

    /**
     * Set a message recipients
     *
     * @param  ?User[] $users List of recipients. Null means everyone.
     * @return ActionTrigger
     */
    public function to(?array $users = null): ActionTrigger
    {
        // Sugar to simplify specifying the recipients
        return new ActionTrigger($users);
    }

    public function toUser(User $user): ActionTrigger
    {
        return $this->to([$user]);
    }

    public function toAdmins(): ActionTrigger
    {
        return $this->to($this->getConnectedAdmins());
    }

    public function toCurrent(): ActionTrigger
    {
        return $this->to([$this->getCurrentUser()]);
    }

    public function toEveryone(): ActionTrigger
    {
        return new ActionTrigger();
    }

    protected function formatMessage(string $type, ?array $data = null): string
    {
        $users = $this->getPlayingUsers();

        $data = $data ? (array) $data : [];
        $data += [
            'players' => collect($users)
                ->map(function (User $user) {
                    return [
                        'name' => $user->getName(),
                        'score' => $user->getScore(),
                    ];
                })->all(),
            'connectedPlayers' => collect($users)
                ->filter(function (User $user) {
                    return $user->isConnected();
                })->map(function (User $user) {
                    return $user->getId();
                })->values()->all()
        ];

        return parent::formatMessage($type, $data);
    }

    /**
     * Define current user as admin
     *
     * @return void
     */
    public function setAdmin(): void
    {
        $user = $this->getCurrentUser();
        $user->isAdmin(true);
    }

    public function getConnectedUsers(): array
    {
        return collect($this->getAllUsers())->filter(function (User $user) {
            return (!empty($user->getName()) || $user->isAdmin()) &&
                    ($user->isConnected());
        })->all();
    }

    public function getConnectedAdmins(): array
    {
        return collect($this->getConnectedUsers())->filter(function (User $user) {
            return $user->isAdmin();
        })->all();
    }

    public function getConnectedPlayingUsers(): array
    {
        return collect($this->getConnectedUsers())->filter(function (User $user) {
            return !$user->isAdmin();
        })->all();
    }

    /**
     * Get all currently connected users
     * Remove admins and anonymous users
     *
     * @return array
     */
    public function getPlayingUsers(): array
    {
        return collect($this->getAllUsers())->filter(function (User $user) {
            return !empty($user->getName()) &&
                    !$user->isAdmin() &&
                    ($user->isConnected() || $user->isPending());
        })->all();
    }

    public function say(string $message): void
    {
        $user = $this->getCurrentUser();
        if ($user !== null) {
            $message = '('.
                ($user->getName() ?
                    var_export($user->getName(), true) :
                    '#'.$user->getId()
                ).
                ($user->isAdmin() ? ' - admin' : '').
            ') :: '.$message;
        }
        parent::say($message);
    }
}

class SocketNode extends Node
{
    protected $_id = null;

    public function setId(string $id): void
    {
        $this->_id = $id;
    }

    public function getId(): string
    {
        return $this->_id;
    }
}
