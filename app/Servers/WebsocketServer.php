<?php

namespace App\Servers;

use App\Exceptions\EventNotFoundException;
use Hoa\Event\Bucket;
use Hoa\Websocket\Node;
use Hoa\Websocket\Server as WebServer;
use Hoa\Socket\Server as SocketServer;

/**
 * Basic Websocket server
 * with connection/disconnection and message exchange
 */
class WebsocketServer
{
    /**
     * @property Bucket
     */
    public $bucket;

    /**
     * Start the websocket server
     *
     * @param  string $host URL of the server and port to listen
     * @return void
     */
    public function start(string $host): void
    {
        $this->getServerInstance($host)->run();
        \Server::say('Server started, listening...');
    }

    /**
     * Get the WebServer instance with the correct settings
     *
     * @param  string $host URL of the server and port to listen
     * @return Hoa\Websocket\Server
     */
    protected function getServerInstance(string $host): WebServer
    {
        $websocket = new WebServer(
            new SocketServer($host)
        );

        $websocket->on('open', function (Bucket $bucket) {
            $this->bucket = $bucket;
            $this->handleConnection();
        });

        $websocket->on('message', function (Bucket $bucket) {
            $this->bucket = $bucket;

            $data = $bucket->getData();
            $this->handleMessage($data['message']);
        });

        $websocket->on('error', function (Bucket $bucket) {
            $this->bucket = $bucket;

            $data = $bucket->getData();
            d('Erreur', $data['exception']);
        });

        $websocket->on('close', function (Bucket $bucket) {
            $this->bucket = $bucket;

            $data = $bucket->getData();
            $this->handleDisconnection($data['code'], $data['reason']);
        });

        return $websocket;
    }

    /**
     * Handle the event connection
     *
     * @return void
     */
    protected function handleConnection(): void
    {
        // Special event,
        // DON'T send any message from this event!
        // It breaks all open sockets!
        $this->handleEvent('connection');
    }

    /**
     * Handle a custom message
     *
     * @return void
     */
    protected function handleMessage(string $message): void
    {
        $messageData = json_decode($message, true);
        $messageType = array_get($messageData, 'type');
        $messageValue = array_get($messageData, 'data');

        if ($messageData !== false) {
            $this->handleEvent($messageType, $messageValue);
        } else {
            trigger_error('Invalid message '.var_export($message, true), E_USER_NOTICE);
        }
    }

    /**
     * Handle the event disconnection
     *
     * @return void
     */
    protected function handleDisconnection($code, $reason): void
    {
        $this->handleEvent('disconnection', array(
            'code' => $code,
            'reason' => $reason
        ));
    }

    /**
     * Handle an event
     *
     * @param  string $eventName
     * @param  mixed  $argument
     * @return void
     */
    protected function handleEvent($eventName, $argument = null): void
    {
        $className = $this->getEventClassName($eventName);
        (new $className())->exec($argument);
    }

    /**
     * Get the Event class name
     *
     * @param  string $eventName
     * @return string
     * @throw EventNotFoundException
     */
    protected function getEventClassName($eventName): string
    {
        $className = 'App\\Servers\\Events\\'.ucfirst($eventName);
        if (class_exists($className)) {
            return $className;
        } else {
            throw new EventNotFoundException('Invalid event '.$eventName);
        }
    }

    /**
     * Send a message to the current node
     *
     * @param  string $type
     * @param  mixed  $data
     * @return void
     */
    public function answer(string $type, ?array $data = null): void
    {
        $this->send($this->getCurrentNode(), $type, $data);
    }

    protected function formatMessage(string $type, ?array $data = null): string
    {
        return json_encode([
            'type' => $type,
            'data' => $data
        ]);
    }

    /**
     * Send a message to a specific node.
     * If no node specified (null),
     * transfer to broadcast.
     *
     * @param  ?Node  $node
     * @param  string $type
     * @param  mixed  $data
     * @return void
     */
    public function send(?Node $node, string $type, ?array $data = null): void
    {
        if ($node === null) {
            $this->broadcast($type, $data);
        } else {
            $this->bucket->getSource()
                ->send($this->formatMessage($type, $data), $node);
        }
    }

    /**
     * Broadcast a message to all connected nodes
     *
     * @param  string $type
     * @param  mixed  $data
     * @return
     */
    public function broadcast(string $type, ?array $data = null): void
    {
        // Broadcast only send to all others nodes
        $this->bucket->getSource()
            ->broadcast($this->formatMessage($type, $data));

        // So also send to current node
        $this->answer($type, $data);
    }

    /**
     * Get current connected node
     *
     * @return Hoa\Websocket\Node
     */
    public function getCurrentNode(): Node
    {
        return $this->bucket->getSource()
            ->getConnection()
            ->getCurrentNode();
    }

    public function say(string $message): void
    {
        echo $message."\n";
    }
}
