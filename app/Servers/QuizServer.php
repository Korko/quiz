<?php

namespace App\Servers;

use App\Traits\SendsActions;
use Questioner;

/**
 * Websocket server
 * with users participating in a quiz
 * with exchange of questions/answers
 */
class QuizServer extends ExchangeServer
{
    const WAITING = 'waiting';
    const QUIZ_TIME = 'questions';
    const SCORES = 'scores';

    public $currentState = self::WAITING;

    public function isQuizWaiting(): bool
    {
        return $this->currentState === self::WAITING;
    }

    public function isQuizStarted(): bool
    {
        return $this->currentState === self::QUIZ_TIME;
    }

    public function isQuizFinished(): bool
    {
        return $this->currentState === self::SCORES;
    }

    public function getCurrentState(): string
    {
        return $this->currentState;
    }

    public function startQuiz(): void
    {
        $this->currentState = self::QUIZ_TIME;
        $this->toEveryone()
            ->sendCurrentState();
    }

    public function endQuiz(): void
    {
        $this->currentState = self::SCORES;
        $this->toEveryone()
            ->sendCurrentState();
    }
}