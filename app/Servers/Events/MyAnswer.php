<?php

namespace App\Servers\Events;

use Questioner;
use Server;

class MyAnswer extends GenericEvent
{
    public function exec(array $data): void
    {
        $question = Questioner::getQuestion($data['question']);

        Server::say('answer '.$question->id.' => '.implode(', ', $data['answer']));
        $user = Server::getCurrentUser();
        $user->addAnswer($question, $data['answer']);

        Server::toAdmins()->sendPlayersAnswered($question);
    }
}
