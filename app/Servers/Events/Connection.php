<?php

namespace App\Servers\Events;

use Server;

class Connection extends GenericEvent
{
    public function exec(): void
    {
        $node = Server::getCurrentNode();

        $userId = uniqid();
        $node->setId($userId);
        $user = Server::addUser($userId);
        $user->connect($node);

        // Say after the connect of the user
        Server::say('connect');

        Server::answer('id', [
            'id' => $userId
        ]);

        // Inform the user about where in the quiz they are
        Server::toUser($user)->sendCurrentState();
    }
}
