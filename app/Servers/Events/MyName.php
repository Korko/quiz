<?php

namespace App\Servers\Events;

use Server;

class MyName extends GenericEvent
{
    public function exec($name): void
    {
        Server::say('name: '.var_export($name, true));

        $user = Server::getCurrentUser();
        if ($user->getName() !== $name) {
            $user->setName($name);

            Server::toUser($user)->sendCurrentState();
        }
    }
}
