<?php

namespace App\Servers\Events;

use Server;

class Ping extends GenericEvent
{
    public function exec(): void
    {
        Server::answer('pong');
    }
}