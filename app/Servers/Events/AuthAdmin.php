<?php

namespace App\Servers\Events;

use Server;
use User;

class AuthAdmin extends GenericEvent
{
    public function exec($code): void
    {
        if ($code === config('socket.admin')) {
            Server::say('admin');
            Server::setAdmin();

            $users = collect(Server::getConnectedPlayingUsers())->each(function (User $user) {
                Server::toCurrent()->sendLogin($user);
            });

            Server::toCurrent()->sendCurrentState();
        }
    }
}
