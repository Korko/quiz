<?php

namespace App\Servers\Events;

use Server;

class MyOldId extends GenericEvent
{
    public function exec(string $oldId): void
    {
        $node = Server::getCurrentNode();

        if (
            Server::hasUser($oldId) &&
            ($oldUser = Server::getUser($oldId))->isDisconnected()
        ) {
            Server::say('oldId '.$oldId);

            Server::getCurrentUser()->copy($oldUser);
            Server::removeUser($oldId);
        }
    }
}