<?php

namespace App\Servers\Events;

use Server;

// Don't send any message from this event!
// It result in breaking all sockets to every user!
class Disconnection extends GenericEvent
{
    public function exec(array $error): void
    {
        Server::say('disconnect');

        $user = Server::getCurrentUser();
        $user->disconnect();
    }
}