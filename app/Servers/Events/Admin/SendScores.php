<?php

namespace App\Servers\Events\Admin;

use App\Servers\Events\GenericEvent;
use Server;

class SendScores extends GenericEvent
{
    public function exec(): void
    {
        Server::say('end quiz');
        Server::endQuiz();
    }
}