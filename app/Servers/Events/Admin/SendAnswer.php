<?php

namespace App\Servers\Events\Admin;

use App\Servers\Events\GenericEvent;
use Server;

class SendAnswer extends GenericEvent
{
    public function exec(): void
    {
        Server::say('answer');
        Server::toEveryone()->sendAnswer();
    }
}