<?php

namespace App\Servers\Events\Admin;

use App\Servers\Events\GenericEvent;
use Server;

class Start extends GenericEvent
{
    public function exec(): void
    {
    	Server::say('start quiz');
        Server::startQuiz();
    }
}