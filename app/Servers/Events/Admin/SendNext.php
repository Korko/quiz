<?php

namespace App\Servers\Events\Admin;

use App\Servers\Events\GenericEvent;
use Server;

class SendNext extends GenericEvent
{
    public function exec(): void
    {
        Server::say('next question');
        Server::toEveryone()->sendNextQuestion();
    }
}