<?php

namespace App\Servers;

use App\Traits\SendsActions;
use Server;
use User;

class ActionTrigger
{
    use SendsActions {
        __call as callAction;
    }

    protected $users;

    /**
     * @param ?User[] $user
     */
    public function __construct(?array $users = null)
    {
        $this->users = $users;
    }

    /**
     * @param string $type
     * @param mixed  $data If Closure, called with the target as single parameter
     */
    public function send(string $type, $data = null): void
    {
        $this->callForEachUser(function (?User $user) use ($type, $data) {
            if (is_callable($data)) {
                $data = call_user_func($data, $user);
            }

            if ($user === null) {
                Server::broadcast($type, $data);
            } else {
                Server::send($user->getNode(), $type, $data);
            }

        });
    }

    public function __call(string $name, array $arguments): void
    {
        $this->callAction($name, array_merge([
            $this->users
        ], $arguments));
    }

    protected function callForEachUser(Callable $callback)
    {
        if ($this->users === null) {
            call_user_func($callback, null);
        } else {
            foreach ($this->users as $user) {
                call_user_func($callback, $user);
            }
        }
    }
}
