<?php

use Illuminate\Support\Debug\Dumper;

if (! function_exists('d')) {
    /**
     * Dump the passed variables.
     *
     * @param  mixed  $args
     * @return void
     */
    function d(...$args)
    {
        foreach ($args as $x) {
            (new Dumper)->dump($x);
        }
    }
}

function say($message)
{
    Server::say($message);
}
