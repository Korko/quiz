<?php

namespace App\Interfaces;

interface Connection
{
    const DISCONNECTED = 0;
    const CONNECTED = 1;
}