<?php

namespace App\Questions;

class Question
{
    public $id;
    public $category;
    public $wording;
    public $answers;
    public $solution;
    public $resolution;
    public $details;
    public $points;

    public function __construct(int $id, string $category, string $wording, array $answers, array $solution, ?string $resolution = 'all', ?string $details, ?int $points = 1)
    {
        $this->id         = $id;
        $this->category   = $category;
        $this->wording    = $wording;
        $this->answers    = $answers;
        $this->solution   = (array) $solution;
        $this->resolution = $resolution;
        $this->details    = $details;
        $this->points     = $points;
    }

    public static function init(array $values): Question
    {
        return new self(
            $values['id'],
            $values['category'],
            $values['wording'],
            $values['answers'],
            (array) $values['solution'],
            array_get($values, 'resolution', 'all'),
            array_get($values, 'details'),
            array_get($values, 'points', 1)
        );
    }

    public function requiredAnswers(): int
    {
        switch ($this->resolution) {
            case 'all':
                return count($this->solution);

            case 'one':
                return 1;

            default:
                trigger_error('Unknown resolution '.var_export($this->resolution, TRUE), E_USER_ERROR);
        }
    }

    public function isCorrectAnswer($answer): bool
    {
        switch ($this->resolution) {
            case 'all':
                return $this->isCorrectAnswerAll((array) $answer);

            case 'one':
                return $this->isCorrectAnswerOne((int) $answer);

            default:
                trigger_error('Unknown resolution '.var_export($this->resolution, TRUE), E_USER_ERROR);
        }
    }

    // We want the answers to be exactly the solution
    protected function isCorrectAnswerAll(array $answer): bool
    {
        return (
            array_diff($answer, $this->solution) === [] &&
            array_diff($this->solution, $answer) === []
        );
    }

    // We want the answer (only one possible) to be one of the solutions
    protected function isCorrectAnswerOne(int $answer): bool
    {
        return (array_search($answer, $this->solution) !== false);
    }

    public function asArray(): array
    {
        return [
            'id'       => $this->id,
            'category' => $this->category,
            'wording'  => $this->wording,
            'answers'  => $this->answers,
            'solution' => $this->solution,
            'details'  => $this->details,
            'points'   => $this->points,
        ];
    }
}
