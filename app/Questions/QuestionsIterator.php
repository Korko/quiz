<?php

namespace App\Questions;

use ArrayIterator;
use CachingIterator;

class QuestionsIterator extends CachingIterator
{
    private $currentQuestion = 0;

    public static function getAllQuestions(): array
    {
        $categories          = config('quiz.questions');
        $categoriesNames     = array_keys($categories);
        $categoriesQuestions = array_values($categories);

        $questions = [];
        for ($offsetCategory = 0; $offsetCategory < count($categories); $offsetCategory++) {
            foreach ($categoriesQuestions[$offsetCategory] as $question) {
                $questions[] = [
                    'id'       => $questions === [] ? 0 : $questions[count($questions)-1]['id'] + 1,
                    'category' => $categoriesNames[$offsetCategory],
                ] + $question;
            }
        }

        return $questions;
    }

    public function __construct()
    {
        // Use CachingIterator to be able to handle hasNext
        parent::__construct(
            new ArrayIterator(self::getAllQuestions()),
            CachingIterator::FULL_CACHE
        );
        $this->next();
    }

    public function getQuestion(int $id): Question
    {
        return Question::init($this->offsetGet($id));
    }

    // Just some sugar methods

    public function current(): ?Question
    {
        return $this->getQuestion($this->key());
    }

    public function hasMore(): bool
    {
        return $this->hasNext();
    }

    public function getCurrent(): ?Question
    {
        return $this->current();
    }

    public function getNext(): ?Question
    {
        $this->next();
        return $this->getCurrent();
    }
}
