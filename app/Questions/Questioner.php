<?php

namespace App\Questions;

// Main question iterator
class Questioner
{
    public function __construct()
    {
        $this->questionIterator = new QuestionsIterator();
    }

    public function __call(string $method, array $parameters)
    {
        return call_user_func(array($this->questionIterator, $method), ...$parameters);
    }
}
