Quiz server real time with several clients

## Nginx ##
To handle tls connection, use Nginx as a bridge between the javascript client and the websocket server.

### Configuration example ###
```
server {
        listen 443 ssl;
        server_name quiz.k0rko.fr;
        root /var/www/k0rko.fr/quiz/public;

        access_log /var/log/nginx/access-k0rko.log;
        error_log /var/log/nginx/error-k0rko.log;

        ssl_certificate     /etc/letsencrypt/live/k0rko.fr/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/k0rko.fr/privkey.pem;

        add_header Strict-Transport-Security "max-age=15552000; includeSubdomains; preload";
        add_header X-Content-Type-Options nosniff;
        add_header X-Frame-Options "SAMEORIGIN" always;
        add_header X-XSS-Protection "1; mode=block";
        add_header X-Robots-Tag none;
        add_header X-Permitted-Cross-Domain-Policies none;

        location / {
                try_files $uri $uri/ /index.php?$query_string;
        }

        location ~ [^/]\.php(/|$) {
                fastcgi_pass php7-handler;
                include fastcgi_params;
        }

        location /ws {
                proxy_pass http://127.0.0.1:8889;
                proxy_http_version 1.1;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection "Upgrade";
        }

        location ~ /\.ht {
                deny all;
        }

        error_page 500 502 503 504 /50x.html;
        location = /50x.html {
                root /var/www/html;
        }
}
```

## Start server ##
To start the socket server, use the following command:
`php artisan socket:start`
