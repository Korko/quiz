<?php

namespace Tests\Unit;

use App\Questions\Question;
use Config;
use Tests\TestCase;

class QuestionTest extends TestCase
{
    public function testBasicInstance()
    {
        $question = $this->fakeQuestion();
        $this->assertTrue($question instanceof Question);
    }
}