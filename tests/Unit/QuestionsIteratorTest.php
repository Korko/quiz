<?php

namespace Tests\Unit;

use App\Questions\QuestionsIterator;
use Config;
use Tests\TestCase;

class QuestionsIteratorTest extends TestCase
{
    public function testBasicIterator()
    {
        $questions = $this->fakeConfigQuestion(2);
        Config::set('quiz.questions', $questions);

        // Ensure we can travel Iterator with a foreach (standard iterator)
        $iterator = new QuestionsIterator();
        $keys = [];
        foreach ($iterator as $key => $question) {
            $this->assertFalse(in_array($key, $keys));
            $this->assertEquals($question->wording, $questions[$key]['wording']);
            $keys[] = $key;
        }

        // Ensure we can travel Iterator with custom methods steps
        // and order is kept
        $iterator = new QuestionsIterator();
        while ($iterator->hasMore()) {
            $question = $iterator->getCurrent();
            $this->assertEquals($question->id, array_shift($keys));
            $this->assertEquals($question->wording, $questions[$question->id]['wording']);
            $this->assertNotEquals($question->id, $iterator->getNext()->id);
        }
    }
}

