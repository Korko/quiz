<?php

namespace Tests;

use Faker\Generator as Faker;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    private function fakeSingleConfigQuestion()
    {
        $faker = app(Faker::class);

        $answers = $faker->sentences($faker->numberBetween(2, 5));
        return [
            'wording'    => $faker->sentence(),
            'answers'    => $answers,
            'solution'   => $faker->randomElement([
                $faker->randomElement(array_keys($answers)),
                $faker->randomElements(array_keys($answers), $faker->numberBetween(2, count($answers) - 1)),
            ]),
            'resolution' => $faker->randomElement(['one', 'all']),
            'details'    => $faker->paragraph(),
        ];
    }

    public function fakeConfigQuestion(?int $quantity = 1)
    {
        $questions = [];
        for ($i = 0; $i < $quantity; $i++) {
            $questions[] = $this->fakeSingleConfigQuestion();
        }
        return $quantity === 1 ? array_pop($questions) : $questions;
    }

    public function fakeQuestion(?int $quantity = 1)
    {
        $questions = [];
        for ($i = 0; $i < $quantity; $i++) {
            $questions[] = Question::init([
                'id' => $i,
            ] + $this->fakeConfigQuestion());
        }
        return $quantity === 1 ? array_pop($questions) : $questions;
    }
}
